fish_add_path ~/.local/bin
fish_add_path ~/.binenv
fish_add_path ~/.cargo/bin

if status is-interactive
    # Commands to run in interactive sessions can go here
    if which starship > /dev/null; starship init fish | source; end
    if which chezmoi > /dev/null; chezmoi completion fish | source; end
    if which flux > /dev/null; flux completion fish | source; end
end
