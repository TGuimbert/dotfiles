# dotfiles

A repository to manage my personal dotfiles with [chezmoi](https://www.chezmoi.io/).

## Getting started

To install and initialize `chezmoi` on a new machine, run:

```shell
BINDIR=~/.local/bin/ sh -c "$(curl -fsLS chezmoi.io/get)" -- init --apply git@gitlab.com:TGuimbert/dotfiles.git
```

## Links

Find commands for daily operations [here](https://www.chezmoi.io/user-guide/daily-operations/).
